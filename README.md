SymfonyRRSkeleton
===

Simple Symfony skeleton based at version 2.8.
Skeleton for Apps considering User management, API component and documentation, StateMachine on entities.

Included Bundles
===

### DoctrineFixturesBundle
    Doctrine Fixtures enabled for DEV and TEST env
    http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
### DoctrineMigrations    
    Doctrine migrations
    http://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html
### FOSJsRoutingBundle
    Allow to include route names at js files
    https://github.com/FriendsOfSymfony/FOSJsRoutingBundle
### FOSRestBundle         
    APIBundle
    https://github.com/FriendsOfSymfony/FOSRestBundle
### FOSUserBundle         
    User Business management (Register, login, forget password...)
    https://github.com/FriendsOfSymfony/FOSUserBundle
### JMSserializerBundle   
    Serializer Bundle
    http://jmsyst.com/bundles/JMSSerializerBundle
### NelmioApiDocBundle    
    Api Doc and sandbox by http://app.tld/api/doc
    https://github.com/nelmio/NelmioApiDocBundle
### PsyshBundle
    Interactive debugging on PHP lvl. (same concept of laravel tinker)
    This bundle is only registered for DEV and TEST env)
    https://github.com/theofidry/PsyshBundle
### WhiteOctoberPagerPhantaBundle
    Pager bundle
    https://github.com/whiteoctober/WhiteOctoberPagerfantaBundle
### Winzou State Machine 
    ORM Entity state machine with callbacks.
    https://github.com/winzou/StateMachineBundle

## Install Guide:
1. Clone Repo 
2. Run "composer install"

## ChangeLog
* version 1

>* Symfony 2.8
>* Initial extra bundles

That's it :)

